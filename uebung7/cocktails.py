import re
import json

def all_ingredients(recipes):
	ingredient_list = []
	for recipe in recipes.itervalues():
		for ingredient in recipe['ingredients'].iterkeys():
			ingredient_normal = normalizeString(ingredient) #normalized ingredient
			#add only unique ingredients to list
			found = 0				
			for zutat in ingredient_list:
				if(zutat == ingredient_normal):
					found = 1
					break
			if(found == 0): ingredient_list.append(ingredient_normal)
	#print all igredients from list
	for ingredient in ingredient_list:		
		print ingredient
	print ("Found " + str(len(ingredient_list)) + " different ingredients")
				
def normalizeString(s):
	temp = ""
	for char in s:
		if(char == "("):
			return temp.lower()[:-1] # remove space at the end
		if(char == ","):
			return temp.lower() # no space to remove
		temp += char
	return temp.lower()

def cocktails_inverse(recipes):
	inv_ingredient = []
	inv_cocktail = []
	for cocktail in recipes.iterkeys(): # for every cocktail
		for ingredient in recipes[cocktail]['ingredients'].iterkeys(): # for every ingredient
			ingredient = normalizeString(ingredient)
			found = 0
			for pos in range(len(inv_ingredient)): # check if ingredient already exists	
				if("\"" + ingredient + "\"" == inv_ingredient[pos]): # add cocktail to list
						inv_cocktail[pos] += ", \"" + escapeChar(cocktail, '"') + "\""
						found = 1
						break
			if(found == 0): # if not found, add ingredient and cocktail to list
				inv_ingredient.append("\"" + ingredient + "\"")
				inv_cocktail.append("\"" + escapeChar(cocktail, '"') + "\"")
				

	# create json dump
	inv_recipes = "{ "
	for pos in range(len(inv_ingredient)):
		inv_recipes += inv_ingredient[pos] + " : ["
		inv_recipes += inv_cocktail[pos] + "],"
	inv_recipes = inv_recipes[:-1] # remove last comma
	inv_recipes += " }"
	# create dict
	recipes = json.loads(inv_recipes)
	# count how often an ingredient is used
	ingredient = []
	count = []
	for val in recipes.iterkeys():
		ingredient.append(val)
		count.append(len(recipes[val]))

	print("The 15 most used ingredients:")
	for j in range(15):	
		highest = 0
		pos = 0
		for i in range(len(count)):
			if(count[i] > highest):
				highest = count[i]
				pos = i
		print (ingredient[pos] + " : " + str(count[pos]) + " times")
		del ingredient[pos]
		del count[pos]

	return recipes

def escapeChar(text, characters):
	for character in characters:
		text = text.replace( character, '\\' + character)
	return text


def possible_cocktails(inverse_recipe, available_ingredients):
	ignore_list = ["eisw\xc3rfel", "crushed ice", "zucker", "cocktailkirschen"]

	possible_cocktails = []

	for ingredient in inverse_recipe.iterkeys():
		for val in inverse_recipe[ingredient]:
			for i in available_ingredients:
				if(i == ingredient):
					possible_cocktails.append(val)
					break
	# possible_cocktails contains all cocktails, which use our ingredients
	# collect all ingredients needed for our cocktails
	for cocktail in possible_cocktail:
		ingredient_needed = []
		for	ingredient in inverse_recipe.iterkeys():
			for val in inverse_recipe[ingredient]:
				if(val == cocktail):
					ingredient_needed.appen(ingredient)
		# now check, if we have the same ingredients
		if(len(ingredient_needed) == len(available_ingredients) + len(ignore_list):
				break
		for i in ingredient_needed:
			if((i == available_ingredients) or ()):
				
	return None

recipes = json.load(open('cocktails.json')) 

all_ingredients(recipes) # how many ingredients have been found

inverse_recipes = cocktails_inverse(recipes) # invert cocktail_list


available_ingredients = []
available_ingredients.append("wodka")
possible_cocktails(inverse_recipes, available_ingredients)

