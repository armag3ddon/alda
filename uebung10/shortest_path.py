#!/usr/bin/env python
# coding: utf-8

#Silvan Lindner
#Alexander Stepanov

import json
import math

cities = json.load(open("entfernungen.json"))

city_to_int = {}
int_to_city = {}

i = 0
for city,data in cities.items():
	city_to_int[city] = i
	int_to_city[i] = city
	i += 1
	
adlst = [None]*len(city_to_int)

for city,data in cities.items():
	temp = []
	for neighbours in data['Nachbarn'].items():
		temp.append([city_to_int[neighbours[0]],neighbours[1]])
		adlst[city_to_int[city]] = temp

############
# AUFGABE 2
		
import heapq	# heapq ist ein Modul von Python
def dijkstra(graph, start, ziel):	# graph: gewichtete Adjazenzliste
	heap = []
	visited = [None]*len(graph)
	visited[start] = start
	for neighbor in graph[start]:
		heapq.heappush(heap, (neighbor[1], start, neighbor[0])) # neighbor[1]:Kantengewicht,neighbor[0]:Endpunkt d. K.
	while len(heap) > 0:	# solange der heap nicht leer ist
		w, fromNode, node = heapq.heappop(heap)
		if visited[node] is not None:	# wenn der kürzeste Pfad bereits bekannt ist, überspringe ihn
			continue
		visited[node] = fromNode    # baue Vorgänger-Baum
		if node == ziel:	# da der heap noch nicht leer ist, wird an dieser Stelle ein break benötigt
			break
		for neighbor in graph[node]:
			if visited[neighbor[0]] is not None:	# wenn der kürzeste Pfad bereits bekannt ist, überspringe ihn
				continue
			heapq.heappush(heap, (neighbor[1]+w, node, neighbor[0]))
	bestPath = []
	t = ziel
	while t != visited[t]:		# Array wird durchlaufen bis der Anker des Pfades gefunden ist, vgl. Union-Search
		bestPath.append(t)
		t=visited[t]
	bestPath.append(start)
	return w,bestPath			# bestPath.reverse()

############
# AUFGABE 3

#"Breite": "50N06", 
#"LÃ¤nge": "08E40"
def luftlinie(koord):
	if (len(koord) != 4):
		print("Nicht genügend Koordinaten vorhanden")
		print("Erwartet wird: [Breite 1, Länge1, Breite 2, Länge 2]")
		return 0
	for i in koord:
		if(len(i) != 5): # Überprüfen auf Länge
			print("Falsches Format in: " + i)
			return 0
		if((not i[:2].isdigit()) or (not i[3:].isdigit())): # Überprüfen auf Zahlen
			print("Falsche Format bei: " + i)
			return 0

	# Überprüfen auf Buchstaben (N und E)
	for i in range(0, len(koord), 2):
		if(koord[i][2] != "N"):
			print("Falsches Format bei: " + koord[i])
			return 0;
		if(koord[i + 1][2] != "E"):
			print("Falsches Format")
			return 0;
	
	grad = [] # grad[0] - breite1 | grad[1] - länge1 | grad[2] - breite2 | grad[3] - länge2
	for i in koord:
		grad.append((float)(i[:2]) + ((float)(i[3:]) / 60))

	for i in range(len(grad)):
		grad[i] = grad[i] / 180 * math.pi

	e = math.sin(grad[0]) * math.sin(grad[2])
	e = e + math.cos(grad[0]) * math.cos(grad[2]) * math.cos(grad[3] - grad[1])
	e = math.acos(e) * 6378.137

	return e

def testdistance(cities):
	for city, data in cities.items():
		sbreite = cities[city]["Koordinaten"]["Breite"] # Startstadt
		slaenge = cities[city]["Koordinaten"][u"Länge"] # Startstadt
		for neighbour, distance in cities[city]["Nachbarn"].items():
			zbreite = cities[neighbour]["Koordinaten"]["Breite"] # Zielstadt
			zlaenge = cities[neighbour]["Koordinaten"][u"Länge"] # Zielstadt
			tmp = luftlinie([sbreite, slaenge, zbreite, zlaenge])
			if(tmp > (float)(distance)):
				print(u"Die Straßen-Distanz zwischen " + city + " und " + neighbour + u" beträgt: " + str(distance))
				print("Per Luftlinie aber: " + str(tmp))

def getfirstitem(liste): # wird fürs sortieren gebraucht
	return liste[0]

def astar(graph, start, ziel):	# graph: gewichtete Adjazenzliste
	heap = []
	visited = [None]*len(graph)
	visited[start] = start
	for neighbor in graph[start]:
		heapq.heappush(heap, (neighbor[1], start, neighbor[0]))
	while len(heap) > 0:
# sortiere heap um
		l_dist = []

		for i, j, k in heap: # i - dist, j - start, k - ziel
			luft = luftlinie([cities[int_to_city[j]]["Koordinaten"]["Breite"], cities[int_to_city[j]]["Koordinaten"][u"Länge"], cities[int_to_city[k]]["Koordinaten"]["Breite"], cities[int_to_city[k]]["Koordinaten"][u"Länge"]])
			l_dist.append([luft, i, j, k])	
		
		l_dist_sorted = sorted(l_dist, key=getfirstitem)

		while len(heap) > 0:
			i, j, k = heapq.heappop(heap)

		for i in range(len(l_dist)):
			heapq.heappush(heap, (l_dist_sorted[i][1], l_dist_sorted[i][2], l_dist_sorted[i][3]))
# heap ist soritert
		w, fromNode, node = heapq.heappop(heap)
		if visited[node] is not None:
			continue
		visited[node] = fromNode
		if node == ziel:
			break
		for neighbor in graph[node]:
			if visited[neighbor[0]] is not None:
				continue
			heapq.heappush(heap, (neighbor[1]+w, node, neighbor[0]))
	bestPath = []
	t = ziel
	while t != visited[t]:
		bestPath.append(t)
		t=visited[t]
	bestPath.append(start)
	return w,bestPath			# bestPath.reverse()

def distance(best_path):
	distance = 0
	for i in range(len(adlst) - 1):
		distance += adlst[best_path[i]][best_path[i+1]][1]
	return distance

def shortestPath(city1,city2):
	distance,best_path = dijkstra(adlst,city_to_int[city1],city_to_int[city2])
	#print best_path
	print 'Der beste Weg von',city2,'nach',city1,'('+str(distance)+'km)','lautet wie folgt: '
	for i in best_path[:-1]:
		print int_to_city[i],'->',
	print int_to_city[best_path[-1]],'\n'


def Aufgabe2():
	shortestPath('Aachen','Passau')
	shortestPath(u'Saarbrücken','Leipzig')
	shortestPath(u'München','Greifswald')
	shortestPath('Konstanz','Kassel')

#Aufgabe2()
print dijkstra(adlst,city_to_int["Berlin"],city_to_int["Heidelberg"])
#print astar(adlst,city_to_int["Berlin"],city_to_int["Heidelberg"])
