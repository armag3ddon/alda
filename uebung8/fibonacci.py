#!/usr/bin/env python
# coding: utf-8

#Silvan Lindner
#Alexander Stepanov



def fib1(n):                      # Funktion berechnet die n-te Fibonacci-Zahl
	if n <= 1: 
		return n                 # Rekursionsabschluss
	return fib1(n-1) + fib1(n-2)  # Baumrekursion
	
def fib3(n):
	f1, f2 = fib3Impl(n)    # Hilfsfunktion, f1 ist die Fibonacci-Zahl von (n+1) und f2 ist die Fibonacci-Zahl von n
	return f2

def fib3Impl(n):
	if n == 0: 
		return 1, 0         # gebe die Fibonacci-Zahl von 1 und die davor zur�ck
	else:                          # rekursiver Aufruf
		f1, f2 = fib3Impl(n-1)      # f1 ist Fibonacci-Zahl von n, f2 die von (n-1)
		return f1 + f2, f1          # gebe neue Fibonacci-Zahl fn+1 = f1+f2 und die vorherige (fn = f1) zur�ck.
	   
	   
def fib5(n):
	f1, f2 = 1, 0                # f1 ist die Fibonaccizahl f�r n=1, f2 die f�r n=0
	while n > 0:
		f1, f2 = f1 + f2, f1     # berechne die n�chste Fibonaccizahl in f1 und speichere die letzte in f2
		n -= 1
	return f2
	
	
import numpy as np

def fib6(n):
	#x = np.matrix(((1,1),(1,0)),dtype='uint64')	# -> Rundungsfehler, Zahlen zu lang
	x = [[1,1],[1,0]]
	if n == 0:
		return 0
	elif n == 1:
		return 1
	elif n % 2 == 0:
		z = power_2(mult_2(x,x),n/2)
		#z = (x*x)**(n/2)
		#return z[0,1]
		return z[0][1]
	elif n % 2 == 1:
		z = mult_2(x,power_2(mult_2(x,x),(n-1)/2))
		#z = x*((x*x)**((n-1)/2))
		#return z[0,1]
		return z[0][1]
		
def power_2(matrix,n):
	result = [[1,0],[0,1]]
	for i in range(n):
		result = mult_2(result,matrix)
	return result

def mult_2(matrix1,matrix2):
	result = [[matrix1[0][0]*matrix2[0][0]+matrix1[0][1]*matrix2[1][0],matrix1[0][0]*matrix2[0][1]+matrix1[0][1]*matrix2[1][1]],[matrix1[1][0]*matrix2[0][0]+matrix1[1][1]*matrix2[1][0],matrix1[1][0]*matrix2[0][1]+matrix1[1][1]*matrix2[1][1]]]
	return result
		
def TEST():
	for i in range(1000):
		fibo5 = fib5(i)
		fibo6 = fib6(i)
		if fibo5 != fibo6:
			print 'FEHLER'