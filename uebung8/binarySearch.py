
def binarySearch2(a, key):
	if len(a) == 0:
		return None
	center = len(a) / 2
	if key == a[center]:
		return center
	elif key < a[center]:
		return binarySearch2(a[:center], key)
	else:
		res = binarySearch2(a[center+1:], key)
		if res is None:
			return None
		else:
			return res + center + 1

def binarySearch(a, key, start, end):
    size = end - start
    if size <= 0:
        return None
    center = (start + end) / 2
    if key == a[center]:
        return center
    elif key < a[center]:
        position = start
        while a[position] < key:
            position = position + 1
        return position
    else:
        position = center + 1
        while a[position] < key:
            position = position + 1
        return position

#c = [1, 3, 5, 8, 11, 12, 13, 15, 17]
#print binarySearch(c, 5, 0, 9)
#print binarySearch2(c, 5)

import unittest
import random
class checkbinarySearch(unittest.TestCase):
	def setUp(self):
		self.emptyarray = [] # empty array
		self.array1 = [1, 3, 5, 8, 11, 12, 13, 15, 17] # a random array
		self.array2 = [30, 31, 32, 500, 800, 808] # a random array
		self.array3 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 200, 300, 400, 500,  600] # a random array
		self.key1 = 5 # random value from first array
		self.key2 = 500 # random value from second array
		self.key3 = 9 # random value from third array

	def test_empty(self):
		self.assertEqual(binarySearch(self.emptyarray, 0, 0, 0), binarySearch2(self.emptyarray, 0))

	def test_binarySearch(self): # we test if binarySearch and binarySearch2 return the same result
		self.assertEqual(binarySearch2(self.array1, self.key1), binarySearch(self.array1, self.key1, 0, len(self.array1)))
		self.assertEqual(binarySearch2(self.array2, self.key2), binarySearch(self.array2, self.key2, 0, len(self.array2)))
		self.assertEqual(binarySearch2(self.array3, self.key3), binarySearch(self.array3, self.key3, 0, len(self.array3)))
		

if __name__ == '__main__':
	unittest.main()
