#!/usr/bin/env python
# coding: utf-8

#Silvan Lindner
#Alexander Stepanov


from math import sqrt
import numpy as np
import copy

def unterteile(p1,p2):	#Unterteilungsregel
	p1_neu = p1 + 1/3.0*(p2 - p1)
	p3_neu = p1 + 2/3.0*(p2 - p1)
	temp = np.array([-(p3_neu - p1_neu)[1],(p3_neu - p1_neu)[0]])
	p2_neu = p1 + 1/2.0*(p2 - p1) + sqrt(3)/2.0*temp
	return [p1,p1_neu,p2_neu,p3_neu]
	


def kochSnowflake(level):
	p1 = np.array([0,0])	#bestimme Anfangspunkte
	p2 = np.array([0,1])
	p3 = np.array([sqrt(3)/2,0.5])
	Punkte = [p1,p2,p3]
	for i in range(level):	
		Punkte_temp = []
		for j in range(len(Punkte)):	#f�r alle Punktepaare in "Punkte"
			if (j == (len(Punkte) - 1)):
				temp = unterteile(Punkte[-1],Punkte[0])	#verkn�pfe letztes und erstes Element
			else:
				temp = unterteile(Punkte[j],Punkte[j+1])
			Punkte_temp.extend(temp)
		Punkte = copy.deepcopy(Punkte_temp)	
	return Punkte
	
	
	
def Aufgabe1(level):
	Punkte = kochSnowflake(level)
	outfile = open("snowflake.txt","w")	#speichere das Ergebnis in "snowflake.txt"
	for i in range(len(Punkte)):
		outfile.write(str(Punkte[i][0])+' '+str(Punkte[i][1])+'\n')
	outfile.close()