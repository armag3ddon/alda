#!/usr/bin/env python
# coding: utf-8

#Silvan Lindner
#Alexander Stepanov


import random
from math import sqrt
from math import trunc
import copy
import timeit

#M Bucketzahl
	
def createData(size):
	a = []
	while len(a) < size:
		x, y = random.uniform(-1, 1), random.uniform(-1, 1)
		r = sqrt(x**2 + y**2)
		if r < 1.0:
			a.append(r)
	return a
	
def FalseBucketMap(array,M):	#a->M*a
	for i in range(len(array)):
		array[i] = array[i]*M
	return array	
	
def BucketMap(array,M):			#a->M*a^2
	for i in range(len(array)):
		array[i] = array[i]*array[i]*M
	return array
	

	
#bucketSort mit BucketMap
def bucketSort1(array,M):
	if (type(array) != list) or (type(M) != int):
		raise TypeError

	bucket = [[]*M for x in xrange(M)]
	array_map = copy.deepcopy(array)
	array_map = BucketMap(array_map,M)
	result = []
	for i in range(len(array)):	#buckets f�llen
		if array_map[i] == M:
			raise ValueError
		bucket[trunc(array_map[i])].append(array[i])
	for i in range(M):
		bucket[i].sort()	#einzelnde buckets sortieren
		result.extend(bucket[i])
	return result
		
		
		
#bucketSort mit FalseBucketMap		
def bucketSort2(array,M):
	if (type(array) != list) or (type(M) != int):
		raise TypeError
	bucket = [[]*M for x in xrange(M)]
	array_map = copy.deepcopy(array)
	array_map = FalseBucketMap(array_map,M)
	result = []
	for i in range(len(array)):	#buckets f�llen
		if array_map[i] == M:
			raise ValueError
		bucket[trunc(array_map[i])].append(array[i])
	for i in range(M):
		bucket[i].sort()	#einzelnde buckets sortieren
		result.extend(bucket[i])
	return result

		
def TestConsistence(size,M,fun):
	array = createData(size)
	array = fun(array,M)
	array.sort()
	N = len(array)

	#eta's berechnen
	eta = [0]
	for k in range(1,M+1):
		i = 0
		while (i < len(array)) and (array[i] < k) :
			i += 1
		eta.append(i-sum(eta[:k]))

	Chi = 0
	for k in range(1,M+1):
		Chi += (eta[k] - N/float(M))**2/(N/float(M))
	teta = sqrt(2*Chi) - sqrt(2*M-3)
	return teta
	

		
def Aufgabe2b():
	print 'BucketMap: a-> M*a^2'
	fehler = 0
	for i in range(100):	#sortiere 100 mal ein array mit zuf�lliger l�nge und berechne teta
		random.randint(0,9)
		test = TestConsistence(1000,random.randint(1,1000),BucketMap)
		if abs(test) > 3:
			print 'FEHLER! (|teta|=',abs(test),')'
			fehler = 1
	if fehler == 0:
		print 'keine Fehler'

		
	print 'FalseBucketMap: a -> M*a'
	fehler = 0
	for i in range(100):	#sortiere 100 mal ein array mit zuf�lliger l�nge und berechne teta
		random.randint(0,9)
		test = TestConsistence(1000,random.randint(1,1000),FalseBucketMap)
		if abs(test) > 3:
			print 'FEHLER! (|teta|=',abs(test),')'
			fehler = 1
	if fehler == 0:
		print 'keine Fehler'
		
		
		
		
import unittest
class checkBucketSort(unittest.TestCase):
	def setUp(self):
		self.unsorted = []
		zufallszahl = random.randint(1,100)
		self.unsorted = createData(zufallszahl)
		self.sorted = copy.deepcopy(self.unsorted)
		self.sorted.sort()
		self.M = random.randint(1,len(self.sorted)-1)
		self.presorted = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]
		self.empty = []
		self.ValueError = [0,0.5,1]	
		
	def test_sort(self): # = checkSorting (L�nge, sortiert, gleiche Elemente)
		temp = bucketSort1(self.unsorted,self.M)
		self.assertEqual(self.sorted, temp)
		
	def test_presorted(self):
		temp = bucketSort1(self.presorted,5)
		self.assertEqual(self.presorted,temp)
		
	def test_empty(self):
		temp = bucketSort1(self.empty,0)
		self.assertEqual(self.empty,temp)
		
	def test_raises(self): 
		self.assertRaises(TypeError,bucketSort1,'string',self.M)		
		self.assertRaises(TypeError,bucketSort1,self.unsorted,'string')		
		self.assertRaises(ValueError,bucketSort1,self.ValueError,self.M)		

#if __name__ == '__main__':
#	unittest.main()
	
	
	
def zeitmessung():
	BucketMap_Time = []
	FalseBucketMap_Time = []
	index = []
	for i in range(1,10):
		index.append(2**i)
	for N in index:	
		#N = 10
		M = (N/4)+1
		array = createData(N)
		
		t1 = timeit.Timer("bucketSort1("+str(array)+","+str(M)+")", setup = "from __main__ import bucketSort1")
		BucketMap_Time.append(t1.timeit(N)/float(N))
		
		t2 = timeit.Timer("bucketSort2("+str(array)+","+str(M)+")", setup = "from __main__ import bucketSort2")
		FalseBucketMap_Time.append(t2.timeit(N)/float(N))

	
	return BucketMap_Time, FalseBucketMap_Time
	

	
