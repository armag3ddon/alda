﻿# -*- coding: utf-8 -*-
#c) Die Bäume sind nicht gleich, da zufällige Wahrscheinlichkeiten
# wohl sehr selten mit der tatsächlichen Worthäufigkeit übereinstimmen.
#d) Tiefe eines perfekt balancierten Baumes: log(165589 Wörter)/log(Binärbaum)=18
# Tiefe dynamischer Treap: 38
# Tiefe zufälliger Treap: 32
# die Tiefen sind beide logischweise größer als die eines perfekt balancierten Baumes

# mittlere Zugriffszeit des dynamischen Treaps: 10.895294977323372
# mittlere Zugriffszeit des zufälligen Treaps: 17.31371045178122
# Ja, es wird bestätigt, dass der dynamische Treap im Durchschnitt schneller ist.
import random
import math
import sys

class Node:
	def __init__(self, key, priority):
		self.key = key
		self.priority = priority
		self.left = None
		self.right = None

	def treapInsert(rootnode, key, priority):
		if rootnode.treeHasKey(key) != None: return
		while True: 
			if rootnode.key == key: return rootnode
			if key<rootnode.key: 
				if rootnode.left: 
					rootnode = rootnode.left
				else: rootnode.left = Node(key, priority)
			else: 
				if key > rootnode.key: 
					if rootnode.right: 
						rootnode = rootnode.right
					else: rootnode.right = Node(key, priority)

	def treeRemove(rootnode, key):
		parent,rotation = rootnode.getRoot(key)
		if parent: 
			if rotation == "left": parent.left = None
			else: parent.right = None

	def getRoot(rootnode, key):
		if rootnode.key == key: return None, None
		while True: 
			if key < rootnode.key: 
				if not rootnode.left: return None, None
				if rootnode.left.key == key: return rootnode, "left"
				rootnode = rootnode.left
			else: 
				if key > rootnode.key: 
					if not rootnode.right: return None, None
					if rootnode.right.key == key: return rootnode, "right"
					rootnode = rootnode.right

	def treeHasKey(rootnode, key):
		while True: 
			if rootnode == None: return None
			if rootnode.key == key: return rootnode
			if key<rootnode.key: 
				rootnode = rootnode.left
			else: 
				if key > rootnode.key: 
					rootnode = rootnode.right

	def keyDepth(rootnode, key):
		depth = 1
		while True: 
			if rootnode == None: return 0
			if rootnode.key == key: return depth
			if key < rootnode.key: 
				rootnode = rootnode.left
			else: 
				if key > rootnode.key: 
					rootnode = rootnode.right
			depth += 1

	def strLevel(self, level):
		if level == 0: return str(self.key) + "(" + str(self.priority) + ")"
		s = ""
		if self.left: s += self.left.strLevel(level - 1)
		else: s += self.dotLevel(level - 1)
		s += " "
		if self.right: s += self.right.strLevel(level - 1)
		else: s += self.dotLevel(level - 1)
		return s

	def dotLevel(self, level): 
		if level > 1: return "." + str(int(math.pow(2, level - 1))) + ". "
		if level == 1: return ".. "
		if level == 0: return ". "

	def __str__(self):
		s = str(self.key) + "("
		if self.left: s += str(self.left.key)
		s += ","
		if self.right: s += str(self.right.key)
		return s + ")"

	def printTreap(self):
		s = ""
		for i in range(treeDepth(self)):
			s += self.strLevel(i)
			s += "\n"
		print(s)

def treeDepth(rootnode):
	if not isinstance(rootnode, Node): raise RuntimeError("cannot determine treeDepth of non-Node Type")
	depthL = 1
	depthR = 1
	if rootnode.left:
		depthL += treeDepth(rootnode.left)
	if rootnode.right:
		depthR += treeDepth(rootnode.right)
	if depthL >= depthR: return depthL
	else: return depthR

def treeRotateLeft(rootnode, node):
	if not isinstance(rootnode, Node) or not isinstance(node, Node):
		raise TypeError("cannot rotate tree of non-Node Type")
	if not isinstance(node.right, Node): 
		raise ValueError("cannot rotate left, node has no right child")
	parent, rotation = rootnode.getRoot(node.key)
	newRoot = node.right
	node.right = newRoot.left
	newRoot.left = node
	if parent != None:
		if rotation == "left": parent.left = newRoot
		else: parent.right = newRoot
		return rootnode
	return newRoot

def treeRotateRight(rootnode, node):
	if not isinstance(rootnode, Node) or not isinstance(node, Node):
		raise TypeError("cannot rotate tree of non-Node Type")
	if not isinstance(node.left, Node): 
		raise ValueError("cannot rotate right, node has no left child")
	parent,rotation = rootnode.getRoot(node.key)
	newRoot = node.left
	node.left = newRoot.right
	newRoot.right = node
	if parent != None:
		if rotation == "left": parent.left = newRoot
		else: parent.right = newRoot
		return rootnode
	return newRoot

def autoRotate(rootnode, node): 
	if not isinstance(rootnode, Node) or not isinstance(node, Node):
		raise TypeError("cannot rotate tree of non-Node Type")
	while True: 
		parent,position = rootnode.getRoot(node.key)
		if parent == None or node.priority <= parent.priority: break
		if position == "left": rootnode = treeRotateRight(rootnode, parent)
		else: rootnode = treeRotateLeft(rootnode, parent)
	return rootnode

def randomTreapInsert(rootnode, key):
	if rootnode == None: rootnode = Node(key, random.randint(0, sys.maxsize))
	else: rootnode.treapInsert(key, random.randint(0, sys.maxsize))
	return autoRotate(rootnode, rootnode.treeHasKey(key))

def dynamicTreapInsert(rootnode, key): 
	if rootnode == None: rootnode = Node(key,0)
	node = rootnode.treeHasKey(key)
	if node:
		node.priority += 1
	else:
		rootnode.treapInsert(key, 1)
	return autoRotate(rootnode, rootnode.treeHasKey(key))

def checkTreap(rootnode, rootnode2):
	res = True
	res = res and rootnode.key == rootnode2.key
	if rootnode.left:
		res = res and checkTreap(rootnode.left, rootnode2.left)
	if rootnode.right:
		res = res and checkTreap(rootnode.right, rootnode2.right)
	return res

def calcP(dynamicTreap, randomTreap, depth):
	global sumP, sumRPd, sumDPd
	sumP += dynamicTreap.priority
	sumDPd += dynamicTreap.priority * depth
	sumRPd+=dynamicTreap.priority * randomTreap.keyDepth(dynamicTreap.key)
	if dynamicTreap.left:
		calcP(dynamicTreap.left, randomTreap, depth + 1)
	if dynamicTreap.right:
		calcP(dynamicTreap.right, randomTreap,depth + 1)
n = None
for i in range(20):
	n = randomTreapInsert(n, i)
	n.printTreap()

s = open('die-drei-musketiere.txt').read() # File einlesen
for k in ',.:-"\'!?':
	s = s.replace(k, '') # Sonderzeichen entfernen
s = s.lower() # alles klein schreiben
text = s.split() # string in array von Wörtern umwandeln
randomTreap = None
dynamicTreap = None
counter = 0
for word in text:
	counter += 1
print(counter)
for word in text:
	randomTreap = randomTreapInsert(randomTreap, word)
	dynamicTreap = dynamicTreapInsert(dynamicTreap, word)
	counter -= 1
	if counter % 10000 == 0: print(counter)

print("Bäume sind gleich?:" + str(checkTreap(randomTreap, dynamicTreap)))
	
for i in range(7):
	print(dynamicTreap.strLevel(i))

print("Tiefe des dynamischen Treaps: " + str(treeDepth(dynamicTreap)))
print("Tiefe des zufälligen Treaps: " + str(treeDepth(randomTreap)))
sumP = 0
sumRPd = 0
sumDPd = 0
calcP(dynamicTreap, randomTreap, 1)
tR = sumRPd / sumP
tD = sumDPd / sumP
print("mittlere Zugriffszeit des dynamischen Treaps: " + str(tD))
print("mittlere Zugriffszeit des zufälligen Treaps: " + str(tR))

import unittest
class checkTreap(unittest.TestCase):
	#def setUp(self):
	
	def testRandom(self): 
		elements=[]
		elements.append(random.randint(0,100))
		node=Node(elements[0],random.randint(0,100))
		for i in range(1,100):
			n = randomTreapInsert(node,random.randint(0,100))
		#check if all elements are still present
		for element in elements:
			self.assertTrue(node.treeHasKey(element))
		#check if all child have lower priorities than their parents
		for element in elements:
			parent,rotation=node.getRoot(element)
			if parent==None: continue
			self.assertTrue(node.treeHasKey(element).priority < parent.priority)
			if rotation=="left":
				self.assertTrue(node.treeHasKey(element).key <= parent.key)
			else: self.assertTrue(node.treeHasKey(element).key >= parent.key)
	
	def testDynamic(self): 
		elements=[]
		elements.append(random.randint(0,100))
		node=Node(elements[0],random.randint(0,100))
		for i in range(1,100):
			n = dynamicTreapInsert(node,random.randint(0,100))
		#check if all elements are still present
		for element in elements:
			self.assertTrue(node.treeHasKey(element))
		#check if all child have lower priorities than their parents
		for element in elements:
			parent,rotation=node.getRoot(element)
			if parent==None: continue
			self.assertTrue(node.treeHasKey(element).priority < parent.priority)
			if rotation=="left":
				self.assertTrue(node.treeHasKey(element).key <= parent.key)
			else: self.assertTrue(node.treeHasKey(element).key >= parent.key)
	
if __name__ == '__main__':
	unittest.main()
