def absteigend(array):
	class functor1:
		def __call__(self, a, b):
			if(a < b): return 1
			if(b < a): return -1
			else: return 0

	myOrdering = functor1()
	array.sort(myOrdering)

	print("Absteigend sortiert:")
	print(array)

def betrag_absteigend(array):
	class functor2:
		def __call__(self, a, b):
			if(abs(a) < abs(b)): return -1
			if(abs(b) < abs(a)): return 1
			else: return 0
			
	myOrdering = functor2()
	array.sort(myOrdering)

	print("Betrag aufsteigend sortiert:")
	print(array)

#gerade aufsteigend, ungerade absteigend
def ger_auf_unger_ab(array):
	class functor3:
		def __call__(self, a, b):
			if((a % 2 == 0) and (b % 2 != 0)): return -1
			if((a % 2 != 0) and (b % 2 == 0)): return 1
			else: return 0

	class functor4:
		def __call__(self, a, b):
			if((a < b) and (a % 2 == 0) and (b % 2 == 0)): return -1
			if((b < a) and (a % 2 == 0) and (b % 2 == 0)): return 1
			if((a % 2 == 0) and (b % 2 != 0)): return 0
			if((a < b) and (a % 2 != 0) and (b % 2 != 0)): return 1
			if((b < a) and (a % 2 != 0) and (b % 2 != 0)): return -1
			else: return 0

	myOrdering = functor3()
	array.sort(myOrdering)

	myOrdering = functor4()
	array.sort(myOrdering)

	print("Gerade aufsteigend und ungerade absteigend sortiert:")
	print(array)

def permutation_sort(array):
	class PermutationSortFunktor:
		def __call__(self, a, b):
			if(read_only_array[a] < read_only_array[b]): return -1
			if(read_only_array[b] < read_only_array[a]): return 1
			else: return 0
		
	read_only_array = array

	permutation = range(len(read_only_array))

	f = PermutationSortFunktor()
	permutation.sort(f)

	array = []

	for k in range(len(read_only_array)):
		array.append(read_only_array[permutation[k]])

	print("Ueber Permutation aufsteigend sortiert:")
	print(array)

	return permutation

#unittest
import unittest
import random
import copy

class checkgeneric_sort(unittest.TestCase):
	def setUp(self):
		self.emptyarray = []
		self.unsorted = []

		rand = random.randint(10, 100)
		for i in range(rand): 
			self.unsorted.append(random.randint(-rand, rand))
		self.sorted = copy.deepcopy(self.unsorted)

	def test_empty(self):
		absteigend(self.emptyarray)
		for i in range(len(self.emptyarray) - 1): 
			self.assertTrue(self.emptyarray[i + 1] < self.emptyarray[i])

		betrag_absteigend(self.emptyarray)
		for i in range(len(self.emptyarray) - 1): 
			self.assertTrue(abs(self.emptyarray[i]) < abs(self.emptyarray[i + 1]))

		ger_auf_unger_ab(self.emptyarray)
		for i in range(len(self.emptyarray) - 1): 
			if((self.emptyarray[i] % 2 == 0) and (self.emptyarray[i + 1] % 2 == 0)): 
				self.assertTrue(self.emptyarray[i] < self.emptyarray[i + 1])
			if(self.emptyarray[i] % 2 != 0 and self.emptyarray[i + 1] % 2 != 0): 
				self.assertTrue(self.emptyarray[i + 1] < self.emptyarray[i])
			else: a = 42

		permutation = permutation_sort(self.emptyarray)
		for i in range(len(self.emptyarray) - 1): 
			self.assertTrue(self.emptyarray[permutation[i]] < self.emptyarray[permutation[i + 1]])
		print("\n")
		
	def test_generic_sort(self):
		print("Array:")
		print(self.unsorted)
		print("\n")

		absteigend(self.sorted)
		for i in range(len(self.sorted) - 1): 
			self.assertTrue(self.sorted[i + 1] <= self.sorted[i])
		
		betrag_absteigend(self.sorted)
		for i in range(len(self.sorted) - 1): 
			self.assertTrue(abs(self.sorted[i]) <= abs(self.sorted[i + 1]))

		ger_auf_unger_ab(self.sorted)
		for i in range(len(self.sorted) - 1): 
			if((self.sorted[i] % 2 == 0) and (self.sorted[i + 1] % 2 == 0)): 
				self.assertTrue(self.sorted[i] <= self.sorted[i + 1])
			if((self.sorted[i] % 2 != 0) and (self.sorted[i + 1] % 2 != 0)): 
				self.assertTrue(self.sorted[i+1] <= self.sorted[i])
			else: a = 42

		permutation = permutation_sort(self.sorted)
		for i in range(len(self.sorted) - 1): 
			self.assertTrue(self.sorted[permutation[i]] <= self.sorted[permutation[i + 1]])
		
if __name__ == '__main__':
	unittest.main()
