#Rekursiv
def dfs (g, node, v = 0):
	if v == 0:
		v = [0] * len(g) #visited-Liste
	v[node] = 1 #besuche node
	for t in g[node]: #gehe zu allen Nachbarn
		if v[t] == 0: #falls diese noch nicht besucht
			weg.append(t)
			dfs(g, t, v) #Rekursion
	return weg

#Iterativ
def dfs_it(g, node):
	v = [0] * len(g)
	weg = [] #fuer die ausgabe
	v[node] = 1

	stack = []
	stack.append(g[node]) # push wollte nicht

	while len(stack) > 0:
		tmp = stack.pop()
		for t in tmp:
			if v[t] == 0:
				v[t] = 1;
				weg.append(t)
				if(t != tmp[len(tmp) - 1]): stack.append(tmp)
				stack.append(g[t])
				break;
	return weg

# Aufgabe 3 d)
#dfs_it(g,node) veraendert um sackgassen anzuzeigen, start und ziel anzugeben und beim ziel die suche abzubrechen
def lab_it(g, start, ziel):
	v = [0] * len(g)
	weg = [] #fuer die ausgabe
	v[start] = 1
	
	sackgassen = 0

	stack = []
	stack.append(g[start]) # push wollte nicht

	while len(stack) > 0:
		tmp = stack.pop()
		for t in tmp:
			if t == ziel: # ziel wurde erreicht, also suche abbrechen
				weg.append(t)
				print ("Sackgassen: " + str(sackgassen))
				return weg				
			if v[t] == 0:
				v[t] = 1;
				weg.append(t)
				if(t != tmp[len(tmp) - 1]): stack.append(tmp)
				stack.append(g[t])
				break;
			if ((v[t] == 1) and (len(tmp) == 1)): #sackgasse
				sackgassen += 1
	print ("Sackgassen: " + str(sackgassen))
	return weg

g = [[0], [2,3], [1,4,5], [1,4,6,7], [2,3], [2], [3], [3]] # Testgraph aus der Wiki

weg = [] # muss wegen der rekursion global erzeugt werden da wir einen rueckgabewert haben wollen
print (dfs(g, 7))

print(dfs_it(g, 7))

# Labyrinth-Graph aus Aufgabe 2
lab = [[1], [0,2,3], [1], [1,4,5], [3], [3,6,7], [5], [5,8,10], [7,8,9,10], [8], [7,8,11], [8], [7,8,11], [10,12,13], [11], [11,14,15], [13], [13]];

print(lab_it(lab, 15, 0))
print(lab_it(lab, 0, 15))

