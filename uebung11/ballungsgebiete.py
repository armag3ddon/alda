#!/usr/bin/env python
# coding: utf-8

#Silvan Lindner
#Alexander Stepanov

import json
from pylab import *

cities = json.load(open("entfernungen.json"))

#erstelle funktionen zum umwandeln zwischen stadt und int
city_to_int = {}
int_to_city = {}
i = 0
for city,data in cities.items():
	city_to_int[city] = i
	int_to_city[i] = city
	i += 1
	
#erstelle adlst	
adlst = [None]*len(city_to_int)
for city,data in cities.items():
	temp = []
	for neighbours in data['Nachbarn'].items():
		temp.append([city_to_int[neighbours[0]],neighbours[1]])
		adlst[city_to_int[city]] = temp
		
import heapq
def prim(graph):  #Graphdatenstruktur ist wie bei Dijsktra  
	heap = []                                       
	visited = [False]*len(graph) 
	sum = 0  #wird sp�ter das Gewicht des Spannbaums sein
	r = []  #r ist die L�sung
	visited[0] = True #fixed
	for neighbor in graph[0]:  #willk�rlich 0 als Wurzel gew�hlt
		heapq.heappush(heap, (neighbor[1], 0, neighbor[0]))  #Heap wird gef�llt 
	while len(heap):
		wn, start, ziel = heapq.heappop(heap) 
		if visited[ziel]: continue
		visited[ziel] = True  #wenn visited noch nicht besetzt
		sum += wn  #Addition des Gewichts der aktuellen Kante
		r.append([start, ziel])  #Kante wird an die Lsg. angeh�ngt
		for neighbor in graph[ziel]:
			if visited[neighbor[0]]: continue
			heapq.heappush(heap, (neighbor[1], ziel, neighbor[0]))
	return sum, r

def distance(a,b):	#berechnet Distanz zwischen 2 Knoten
	for i in range(len(adlst[a])):
		if adlst[a][i][0] == b:
			return adlst[a][i][1]
	
	
def clustering(min_tree,threshold):	#durchsucht alle Kanten und l�scht sie, falls die Distanz der beiden Knoten zu gro� ist
	cluster_amount = 1
	for i in range(len(min_tree)-1,0,-1):
		dist = distance(min_tree[i][0],min_tree[i][1])
		if dist >= threshold:
			#del min_tree[i]
			cluster_amount = cluster_amount + 1	#wenn eine Kante gel�scht wird, kommt jeweils ein cluster dazu
	return cluster_amount
	
def Aufgabe1():
	sum,min_tree = prim(adlst)
	threshold = arange(0,150,1)
	cluster_amount = [0]*150
	for i in range(len(threshold)):
		cluster_amount[i] = clustering(min_tree,threshold[i])
	plot(threshold,cluster_amount)
	ylabel('amount of cluster')
	xlabel('threshold')
	show()


