from pgm import readPGM, writePGM
width, height, data = readPGM('cells.pgm')
#writePGM(width, height, data, 'mein_bild.pgm')

############
# Aufgabe 1

def createMask(width, height, data, threshold):
    mask = []
    for y in range(height):
        for x in range(width):
            if(data[x + y*width] < threshold):
                mask.append(0)
            else:
                mask.append(255)
    return mask

mask = createMask(width, height, data, 60)

writePGM(width, height, mask, 'mask.pgm')

#############
# Aufgabe  2

def createGraph(width, height, mask):
    return mask

graph = createGraph(width, height, mask)

